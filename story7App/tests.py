from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
import unittest
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from .views import index

# Create your tests here.
class UnitTest(TestCase):
    def test(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'landingPage.html')

class story7FunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(story7FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(story7FunctionalTest, self).tearDown()

    def test_input_todo(self):
        selenium = self.selenium
        response = Client().get('/')
        selenium.get('http://127.0.0.1:8000/')
        
        idBody = selenium.find_element_by_id('body')
        accAtas = selenium.find_element_by_id('accAtas')
        accTengah = selenium.find_element_by_id('accTengah')
        accBawah = selenium.find_element_by_id('accBawah')
        btnGantiTema = selenium.find_element_by_id('btnGanti')

        time.sleep(1)
        accAtas.click()
        var = accAtas.get_attribute("class")
        self.assertNotIn(var, 'ui-state-active')
        time.sleep(1)

        accTengah.click()
        var2 = accTengah.get_attribute("class")
        self.assertIn('ui-state-active',var2)
        time.sleep(1)

        accBawah.click()
        var3 = accBawah.get_attribute("class")
        self.assertIn('ui-state-active',var3)
        time.sleep(1)

        btnGantiTema.click()
        var4 = idBody.get_attribute("background")
        self.assertIn('/static/images/thumbnail_clouds.jpg',var4)
        time.sleep(1)
        btnGantiTema.click()
        var4 = idBody.get_attribute("background")
        self.assertIn('/static/images/thumbnail_yellowhalftone.jpg',var4)
        time.sleep(1)
