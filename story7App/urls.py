from django.contrib import admin
from django.urls import path, include
from . import views

app_name = 'story7App'

urlpatterns = [
    path('', views.index, name='landingPage'),
]
