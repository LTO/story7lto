$(document).ready(function(){
    $( "#accordion" ).accordion({collapsible: true});
    var changed = true;
    $("#btnGanti").click(function(){
        if(changed){
            console.log("theme changed");
            $("body").attr('background',"/static/images/thumbnail_clouds.jpg");
            $("body").css('background-repeat',"no-repeat");
            changed = false;
        }else{
            console.log("back to original");
            $("body").attr('background',"/static/images/thumbnail_yellowhalftone.jpg");
            $("body").css('background-repeat',"no-repeat");
            changed = true;
        }

    });
});
