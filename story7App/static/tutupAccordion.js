$(document).ready(function () {
    $( "#accordion" ).accordion({collapsible: true});
    /* jika menu collapse diklik */
    $('#accAtas').click(function(){
     
     /* menghentikan proses fitur default collapse jika kita klik menu collapse */
     event.stopPropagation();
   
     /* aksi default dari event ketika link diklik (misal : tidak akan menampilkan data href pada  url */
     event.preventDefault();
     
     /* mencari class dropdown menggunakan jQuery .closest() dan menyinpannya di variable drop */
     var drop = $(this).closest(".dropdown");
   
     /* menseleksi variable drop dan menambahkan class open menggunakan jQuery .addClass() */
     $(drop).addClass("open");

        });
  });